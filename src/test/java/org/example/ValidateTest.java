package org.example;

import org.apache.commons.lang3.Validate;
import org.junit.Assert;
import org.junit.Test;

public class ValidateTest {

    @Test(expected = NullPointerException.class)
    public void testNullGame() {
        Game gta = null;
        Validate.notNull(gta, "Game object cannot be null");
    }

    @Test
    public void testAddition() {
        int a = 5;
        int b = 3;
        Assert.assertEquals("adding three and five",8, a + b);

    }
}
