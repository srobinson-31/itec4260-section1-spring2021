package org.example;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StoreTest {

    private Store gamestop;

    @Before
    public void setUp() {
        gamestop = new Store();
        gamestop.loadInventoryFromWeb("https://pastebin.com/raw/UnZQxLJL");
    }

    @Test
    public void testLoadFromWeb() {
        Assert.assertEquals(6, gamestop.getInventory().size());
    }


    @Test
    public void testFileExists() throws IOException {
        //System.out.println(System.getProperty("user.dir"));
        File f = new File("temp.txt");
        if( f.exists() ) {
            FileUtils.writeStringToFile(f, "line to add \n", "UTF-8", true);
        } else {
            FileUtils.writeStringToFile(f, "created new file \n", "UTF-8");
        }

        List<String> lines = new ArrayList<String>();
        lines.add("one line\n");
        lines.add("two line\n");
        FileUtils.writeLines(f, lines, true);
    }

    @Test
    public void testRead() throws IOException {
        File f = new File("temp.txt");
        String str = FileUtils.readFileToString(f, "UTF-8");
        //System.out.println(str);
        List<String> lines = FileUtils.readLines(f, "UTF-8");
        System.out.println(lines);
    }


}
