package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class CarListingTest {

    private static WebDriver driver;
    private static final String USED_CARS_CRAIGSLIST = "https://atlanta.craigslist.org/d/cars-trucks/search/cta";
    private static Connection connection;
    private static final String DB_URL = "jdbc:sqlite:cars.db";

    @BeforeClass
    public static void setupDriver() throws SQLException {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        connection = DriverManager.getConnection(DB_URL);
    }

    @Test
    public void testFrontPage() {
        driver.get(USED_CARS_CRAIGSLIST);
        List<WebElement> carElements = driver.findElements(By.className("result-row"));
        Assert.assertEquals(120, carElements.size());
        int startCount = getVehicleTableSize();
        // grab title of car listing
        Map<String, Double> carMap = new HashMap<>();
        List<Car> cars = new ArrayList<>();
        for (int i = 0; i < carElements.size(); i++) {
            WebElement currentCarElement = carElements.get(i);
            WebElement anchor = currentCarElement.findElement(By.xpath(".//a[@class='result-title hdrlnk']"));
            String title = anchor.getText();
            System.out.println(title);
            String postURL = anchor.getAttribute("href");
            System.out.println(postURL);
            WebElement priceElement = currentCarElement.findElement(By.xpath(".//span[@class='result-price']"));
            System.out.println(priceElement.getText());
            double price = Double.parseDouble(priceElement.getText().replace("$", "").replace(",", ""));
            carMap.put(title, price);
            Car car = new Car(title, postURL, price, new Date().toString());
            cars.add(car);
            Assert.assertTrue(price >= 0);
            addVehicleToDatabase(title,(int)price,postURL,new Date().toString());  // add to db
        }
        int endCount = getVehicleTableSize();
        Assert.assertTrue(endCount - startCount == 120);
        serializeCars(cars);
        findCheapestCar(carMap);
    }

    private void serializeCars(List<Car> cars) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("cars.json"), cars);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addVehicleToDatabase(String title, int price, String url, String timestamp) {
        String sql = "insert into vehicles (title, price, url, timestamp) values (?, ?, ? , ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, title);
            ps.setInt(2, price);
            ps.setString(3, url);
            ps.setString(4, timestamp);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void findCheapestCar(Map<String, Double> carMap) {
        double cheapest = Double.MAX_VALUE;
        String title = "";
        for (String t : carMap.keySet()) {
            if (carMap.get(t) < 800) continue;
            if (cheapest > carMap.get(t)) {
                cheapest = carMap.get(t);
                title = t;
            }
        }
        System.out.println("The cheapest car is: " + title + "\n" + cheapest);
    }

    public int getVehicleTableSize() {
        String sql = "select count(*) from vehicles";
        int count = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            rs.next();
            count = rs.getInt("count(*)");
            ps.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Test
    public void testVehicleTableSize() {
        int count = getVehicleTableSize();
        System.out.println(count);
        Assert.assertTrue(count >= 0);
    }

    @Test
    @Ignore
    public void testSerialization() throws JsonProcessingException, IOException {
        Car dreamCar = new Car("Mercedes Benz S-Class", "http://mb.com", 100000, new Date().toString());
        ObjectMapper mapper = new ObjectMapper();
        //System.out.println(mapper.writeValueAsString(dreamCar));
        mapper.writeValue(new File("dreamcar.json"), dreamCar);
    }


    @AfterClass
    public static void cleanUp() throws Exception {
        Thread.sleep(7000);
        driver.close();
        connection.close();
    }
}
