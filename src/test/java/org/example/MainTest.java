package org.example;



import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MainTest {
    @Test
    public void testGames() {
        List<String> platform = new ArrayList<>();
        platform.add("PC");
        List<String> genre = new ArrayList<>();
        genre.add("Action RPG");
        // cmd + p or ctrl + p to view parameter list
        Game diablo = new Game(platform, "Diablo", LocalDate.of(1997, 1, 3),
                "Blizzard North", genre, 59.99, "M");
        System.out.println(diablo);
    }

    @Test
    public void testString() {
        String str = StringUtils.capitalize("a");
    }
}
