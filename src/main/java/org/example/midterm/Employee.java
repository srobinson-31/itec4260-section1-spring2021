package org.example.midterm;

public class Employee {
    private String name;
    private long addTime;

    public Employee(String name) {
        this.name = name;
        this.addTime = System.currentTimeMillis();
    }

    public String getName() {
        return name;
    }

    public long getAddTime() {
        return addTime;
    }

    public int getVacationDays() {
        return 20;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", addTime=" + addTime +
                '}';
    }
}
