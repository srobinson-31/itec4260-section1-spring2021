package org.example;

public class Car {
    private String title;
    private String url;
    private double price;
    private String retrievalDate;

    public Car() {
    }

    public Car(String title, String url, double price, String retrievalDate) {
        this.title = title;
        this.url = url;
        this.price = price;
        this.retrievalDate = retrievalDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getRetrievalDate() {
        return retrievalDate;
    }

    public void setRetrievalDate(String retrievalDate) {
        this.retrievalDate = retrievalDate;
    }
}
