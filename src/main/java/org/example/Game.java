package org.example;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;


/**
 * Represents a game for PC, nintendo, playstation etc.
 *
 *
 */
public class Game {

    private List<String> platform;
    private String name;
    private LocalDate releaseDate;
    private String developer;
    private List<String> genre;
    private BigDecimal price;
    private String rating;
    private int score;

    public Game() {
    }

    /**
     * User this constructor to provide a score for the name.
     * For example, 100 for best game ever, 0 for worst game ever.
     *
     * @param platform List of platforms such as PC, PS4, Xbox One, Switch etc
     * @param name The name of the game such as "Elder Scrolls Skyrim"
     * @param releaseDate Month, date, year represented in LocalDate
     * @param developer The name of the developer in a String
     * @param genre List of genres such as RPG, action, puzzle, etc
     * @param price Price of games represented as BigDecimal internally
     * @param rating Critic's rating
     * @param score Number indicating how good the game is
     */
    public Game(List<String> platform, String name, LocalDate releaseDate, String developer, List<String> genre, double price, String rating, int score) {
        this.platform = platform;
        this.name = name;
        this.releaseDate = releaseDate;
        this.developer = developer;
        this.genre = genre;
        this.price = new BigDecimal(price + "");
        this.rating = rating;
        this.score = score;
    }

    /**
     * Construct a Game object that should represent a game such as PC games or console games
     *
     * <pre>
     *     Game game = new Game(new ArrayList(Arrays.asList("PC","PS4")),"Skyrim",LocalDate.of ...
     *
     *
     * </pre>
     *
     *
     * @param platform List of platforms such as PC, PS4, Xbox One, Switch etc
     * @param name The name of the game such as "Elder Scrolls Skyrim"
     * @param releaseDate Month, date, year represented in LocalDate
     * @param developer The name of the developer in a String
     * @param genre List of genres such as RPG, action, puzzle, etc
     * @param price Price of games represented as BigDecimal internally
     * @param rating Critic's rating
     */
    public Game(List<String> platform, String name, LocalDate releaseDate, String developer, List<String> genre, double price, String rating) {
        this.platform = platform;
        this.name = name;
        this.releaseDate = releaseDate;
        this.developer = developer;
        this.genre = genre;
        this.price = new BigDecimal(price + "");
        this.rating = rating;
    }

    public List<String> getPlatform() {
        return platform;
    }

    public void setPlatform(List<String> platform) {
        this.platform = platform;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public double getPrice() {
        return price.doubleValue();
    }

    public void setPrice(double price) {
        this.price = new BigDecimal(price + "");
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Game{" +
                "platform=" + platform +
                ", name='" + name + '\'' +
                ", releaseDate=" + releaseDate +
                ", developer='" + developer + '\'' +
                ", genre=" + genre +
                ", price=" + price +
                ", rating='" + rating + '\'' +
                ", score='" + score + '\'' +
                '}';
    }

    /**
     * Prints out the game information to System.out
     * careful, this is not calling the {@code toString()} method
     *
     */
    public void printGame() {
        // PC
        //Cyberpunk 2077
        //12/10/2020
        //CD Projekt Red Studio
        //Action RPG, Role-Playing
        //$59.99
        //M
        System.out.println(platform);
        System.out.println(name);
        // rest follows...
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return name.equals(game.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
