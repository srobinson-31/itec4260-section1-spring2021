package org.example;

import static org.junit.Assert.assertTrue;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Unit test for simple App.
 */
@RunWith(JUnitParamsRunner.class)
public class AppTest {

    @Test
    @Parameters({"1,1","2,3","5,6"})
    public void helloParametrizedTest(int a, int b) {
        Assert.assertEquals(a + b, add(a, b));
    }

    public int add(int a, int b) {
        return a + b;
    }

}
