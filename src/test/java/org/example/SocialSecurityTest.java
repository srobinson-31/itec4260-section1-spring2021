package org.example;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Some tests to verify values from SSN calculator
 * 4/12/201
 */
@RunWith(JUnitParamsRunner.class)
public class SocialSecurityTest {

    private static WebDriver driver;
    private static String SS_CALC_URL = "https://www.ssa.gov/OACT/quickcalc/";
    private static Connection connection;
    private static final String DB_URL = "jdbc:sqlite:people.db";

    @BeforeClass
    public static void setUpDriver() throws SQLException {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        connection = DriverManager.getConnection(DB_URL);
    }

    @Test
    @Ignore
    @Parameters({"1990,60000", "1991,59000", "1992,58000",
            "1993,57000", "1994,56000", "1995,55000", "1996,54000", "1997,53000", "1998,52000", "1999,51000"})
    public void testLateMillenialPayout(int year, int salary) {
        driver.get(SS_CALC_URL);
        WebElement monthElement = driver.findElement(By.name("dobmon"));
        monthElement.clear();
        monthElement.sendKeys("01");
        WebElement dayElement = driver.findElement(By.name("dobday"));
        dayElement.clear();
        dayElement.sendKeys("01");
        WebElement yearElement = driver.findElement(By.name("yob"));
        yearElement.clear();
        yearElement.sendKeys(year + "");
        WebElement earningsElement = driver.findElement(By.name("earnings"));
        earningsElement.clear();
        earningsElement.sendKeys(salary + "");
        WebElement submitButton = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submitButton.submit();
        // FRA == full retirement age (67)
        WebElement fraElement = driver.findElement(By.cssSelector("#est_fra"));
        String payoutStr = fraElement.getText();
        double payout = Double.parseDouble(payoutStr.replace("$", "").replace(",", ""));
        addRowToRetireeTable(year, salary, (int) payout);
    }

    public void addRowToRetireeTable(int year, int salary, int payout) {
        String sql = "insert into retirees (birth_year, salary, payout) values (?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, year);
            ps.setInt(2, salary);
            ps.setInt(3, payout);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
    2011	106,800
    2012	110,100
    2013	113,700
    2014	117,000
    2015	118,500
    2016	118,500
    2017	127,200
    2018	128,400
    2019	132,900
    2020	137,700
    2021	142,800
     */
    @Test
    @FileParameters("salary.csv")
    public void testMaxPayout(int year, int salary) {
        driver.get(SS_CALC_URL);
        WebElement monthElement = driver.findElement(By.name("dobmon"));
        monthElement.clear();
        monthElement.sendKeys("01");
        WebElement dayElement = driver.findElement(By.name("dobday"));
        dayElement.clear();
        dayElement.sendKeys("01");
        WebElement yearElement = driver.findElement(By.name("yob"));
        yearElement.clear();
        yearElement.sendKeys(year + "");
        WebElement earningsElement = driver.findElement(By.name("earnings"));
        earningsElement.clear();
        earningsElement.sendKeys(salary + "");
        WebElement submitButton = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submitButton.submit();
        // FRA == full retirement age (67)
        WebElement fraElement = driver.findElement(By.cssSelector("#est_fra"));
        String payoutStr = fraElement.getText();
        double payout = Double.parseDouble(payoutStr.replace("$", "").replace(",", ""));
        System.out.println(year + " " + salary + " " + payout);
        //addRowToRetireeTable(year, salary, (int) payout);
    }
    
    @Test
    @Parameters({"1960","1970"})
    public void testMiniRetirement(int yob) {
        driver.get(SS_CALC_URL);
        WebElement monthElement = driver.findElement(By.name("dobmon"));
        monthElement.clear();
        monthElement.sendKeys("01");
        WebElement dayElement = driver.findElement(By.name("dobday"));
        dayElement.clear();
        dayElement.sendKeys("01");
        WebElement yearElement = driver.findElement(By.name("yob"));
        yearElement.clear();
        yearElement.sendKeys(yob + "");
        WebElement earningsElement = driver.findElement(By.name("earnings"));
        earningsElement.clear();
        earningsElement.sendKeys(89000 + "");
        WebElement submitButton = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submitButton.submit();
        int fraAmount = getFRAAmount();
        System.out.println(fraAmount);
        WebElement earningsButton = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[3]/td/form/input[10]"));
        earningsButton.submit();
        // insert 0 earning from ages 22 to 32
        int startYear = yob + 22;
        int endYear = yob + 42;
        for (int i = startYear; i <= endYear; i++) {
            WebElement yearEarning = driver.findElement(By.name(i + ""));
            yearEarning.clear();
            yearEarning.sendKeys("0");
        }
        WebElement gobackButton = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[1]/td[1]/form/input[6]"));
        gobackButton.submit();
        int adjustedFra = getFRAAmount();
        System.out.println(adjustedFra);

    }

    public int getFRAAmount() {
        // FRA == full retirement age (67)
        WebElement fraElement = driver.findElement(By.cssSelector("#est_fra"));
        String payoutStr = fraElement.getText();
        double payout = Double.parseDouble(payoutStr.replace("$", "").replace(",", ""));
        return (int) payout;
    }

}
